sfpMediaplayerYoutube = {

	/**
	 *
	 */
	players: [],

	/**
	 *
	 * @param id
	 * @param videoId
	 * @param autoplay
	 * @param loop
	 * @param controlbar
	 * @param width
	 * @param height
	 */
	addPlayer: function (id, videoId, autoplay, loop, controlbar, width, height) {

		this.players.push({
			id: id,
			autoplay: autoplay !== undefined ? autoplay : 0,
			loop: loop !== undefined ? loop : 0,
			controlbar: controlbar !== undefined ? controlbar : 0,
			height: height !== undefined ? height : 480, // recommended 16:9 - since we use responsive css trick this value has no meaning
			width: width !== undefined ? width : 270, // recommended 16:9 - since we use responsive css trick this value has no meaning
			videoId: videoId,
			modestbranding: 1,
			showinfo: 0
		})

	},

	/**
	 *
	 */
	createPlayers: function () {

		var i = 0;

		for (i = 0; i < this.players.length; i++) {

			var currentPlayerInfo = this.players[i];

			var player = new YT.Player(currentPlayerInfo['id'], {
				height: currentPlayerInfo['height'],
				width: currentPlayerInfo['width'],
				videoId: currentPlayerInfo['videoId'],
				playerVars: {
					autoplay: currentPlayerInfo['autoplay'],
					loop: currentPlayerInfo['loop'],
					playlist: currentPlayerInfo['loop'] == 1 ? currentPlayerInfo['videoId'] : '',
					controls: currentPlayerInfo['controlbar']
				},
				events: {
					'onReady': this.onPlayerReady,
					'onStateChange': this.onPlayerStateChange
				}
			});

		}


	},

	/**
	 *
	 * @param event
	 */
	onPlayerReady: function(event) {

		console.log(event);

		// Todo: autostart
//		event.target.playVideo();

	},

	/**
	 *
	 * @param event
	 */
	onPlayerStateChange: function(event) {

	}

};

function onYouTubeIframeAPIReady() {

	sfpMediaplayerYoutube.createPlayers();

}