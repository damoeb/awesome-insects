/* var clickpathBeginCount = 5; */
/* var maxPages = 10; */
var cookieNamePrefix = "BUHLER_SEO_";
var separator = "-XXX-";
function setTrackingCookie(cookieName, cookieValue) {
    var exdays = "2";
    var sPath = "/";
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    cookieValue = escape(cookieValue) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + ((sPath==null) ? "" : "; path="+sPath);
    document.cookie = cookieNamePrefix + cookieName + "=" + cookieValue;
}
function appendTrackingCookie(cookieName, cookieValue) {
    var newValue;
    var currentValue = getTrackingCookie(cookieName);
    if (typeof currentValue === "undefined"  || currentValue == "") {
        newValue = cookieValue;
    } else {
        newValue = currentValue + separator + cookieValue; 
    }
    setTrackingCookie(cookieName, newValue);
}
function getTrackingCookie(cookieName) {
    cookieName = cookieNamePrefix + cookieName;
    var i,x,y,aCookies = document.cookie.split(";");
    for (i=0;i<aCookies.length;i++) {
        x=aCookies[i].substr(0,aCookies[i].indexOf("="));
        y=aCookies[i].substr(aCookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==cookieName) {
            return unescape(y);
        }
    }
}
function setTrackingInfos() {
    var referrer = "no referrer";
    var referrerQuery = "no query";
    var clickpath = "Clickpath";
    var download = "Download";
    var currentUrl = location.href;
    var currentUrl = currentUrl.replace('http://www.buhlergroup.com/','');
    var currentUrl = currentUrl.replace('http://uzn515.buhlergroup.com/','');
    var currentUrl = currentUrl.replace('http://staging.buhlergroup.com/','');
    // Set Referrer and Search Query
    if (document.referrer.length > 1) {
        referrer = document.referrer;
        if (referrer.indexOf('www.buhlergroup.') != -1  || referrer.indexOf('uzn515.buhlergroup.') != -1  || referrer.indexOf('staging.buhlergroup.') != -1 ) {    
            // Nothing to do
        } else {
            // Clickpath und Dowloads leeren, Refferer page doesn't belong to buhlergroup
            setTrackingCookie(clickpath, "");
            setTrackingCookie(download, "");
            //Referrer is Google
            if (referrer.indexOf('www.google.') != -1) {
                var indexOfQuery = referrer.indexOf('&q=') + 3;
                var cutReferrer1 = referrer.substring(indexOfQuery, referrer.length);
                var indexOfQueryEnd = cutReferrer1.indexOf('&');
                var cutReferrer2 = cutReferrer1.substring(0,indexOfQueryEnd);
                if(cutReferrer2.length>1){
                    //Found Google - Query
                    referrerQuery = unescape(cutReferrer2);
                }
            } else {
                // Nothing to do
            }
            setTrackingCookie("Referrer", referrer);
            setTrackingCookie("SearchQuery", referrerQuery);
        }
        
    };
    //Klickpfade tracken
    var currentClickpath = getTrackingCookie(clickpath);
    if (typeof currentClickpath === "undefined"  || currentClickpath == "") {
        setTrackingCookie(clickpath, currentUrl);
    } else {
        var lastPage;
        if (currentClickpath.indexOf(separator) == -1 ) {
            lastPage = currentClickpath;
        } else {
            lastPage = currentClickpath.substring(currentClickpath.lastIndexOf(separator)+2,currentClickpath.length);
            //Check the path length and delete one entry if maxpages is already reached
            /*var pathArray = currentClickpath.split(separator);
            var count = pathArray.length;
            if (count >= maxPages) {
                currentClickpath = "";
                for(i=0;i<clickpathBeginCount;i++){
                    currentClickpath += pathArray[i] + separator;
                }
                currentClickpath +=  "...";
                var deleteCount = clickpathBeginCount + (count - maxPages) +1;
                for(i=deleteCount;i<count;i++){
                    currentClickpath += separator + pathArray[i] ;
                }
            }*/
        }
        if (lastPage != currentUrl) {
            setTrackingCookie(clickpath, currentClickpath + separator + currentUrl);
        }
    }
}
setTrackingInfos();
function loadTrackingInfos() {
    var trackingInfos = new Array();
    var i,x,y,aCookies = document.cookie.split(";");
    for (i=0;i<aCookies.length;i++) {
        x=aCookies[i].substr(0,aCookies[i].indexOf("="));
        y=aCookies[i].substr(aCookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x.substring(0, cookieNamePrefix.length) == cookieNamePrefix) {
            var trackingInfo = new Array();
            var tCookieType = x.substring(cookieNamePrefix.length,x.length);
            var tCookieValue = unescape(y);
            trackingInfo.push(tCookieType);
            trackingInfo.push(tCookieValue);
            trackingInfos.push(trackingInfo);
            if (tCookieType=="Clickpath" || tCookieType=="Download") {
                var trackingInfo = new Array();
                trackingInfo.push(tCookieType+"#");
                var aCookieValue = tCookieValue.split(separator);
                trackingInfo.push(aCookieValue.length);
                trackingInfos.push(trackingInfo);
            }
        }
    }
    return trackingInfos;
}